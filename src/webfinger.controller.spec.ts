import { Test, TestingModule } from '@nestjs/testing';
import { WebfingerController } from './webfinger.controller';
import { WebfingerService } from './webfinger.service';

describe('WebfingerController', () => {
  let appController: WebfingerController;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [WebfingerController],
      providers: [WebfingerService],
    }).compile();

    appController = app.get<WebfingerController>(WebfingerController);
  });

  describe('Well known', () => {
    it('should return subject with OIDC issuer link', () => {
      expect(appController.getWebfinger('acct:me@domain.com')).toMatchObject({
        subject: 'acct:me@domain.com',
        links: [
          {
            rel: 'http://openid.net/specs/connect/1.0/issuer',
            href: `${process.env.ISSUER_OIDC_URL}`,
          },
        ],
      });
    });
  });
});
