import { Controller, Get, Res } from '@nestjs/common';

@Controller()
export class RootController {
  @Get('/')
  index(@Res() res) {
    res.status(302).redirect(`${process.env.ROOT_REDIRECT_URL}/`);
  }
}
