import { Injectable } from '@nestjs/common';

export type Webfinger = {
  subject: string;
  links: {
    rel: string;
    href: string;
  }[];
};

@Injectable()
export class WebfingerService {
  getWebfinger(resource: string): Webfinger {
    return {
      subject: resource,
      links: [
        {
          rel: 'http://openid.net/specs/connect/1.0/issuer',
          href: `${process.env.ISSUER_OIDC_URL}`,
        },
      ],
    };
  }
}
