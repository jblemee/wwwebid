import { Module } from '@nestjs/common';
import { WebfingerController } from './webfinger.controller';
import { WebfingerService } from './webfinger.service';
import { ConfigModule } from '@nestjs/config';
import { RootController } from './root.controller';

@Module({
  imports: [ConfigModule.forRoot({ isGlobal: true })],
  controllers: [WebfingerController, RootController],
  providers: [WebfingerService],
})
export class AppModule {}
