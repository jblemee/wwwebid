import { Controller, Get, Query } from '@nestjs/common';
import { Webfinger, WebfingerService } from './webfinger.service';

@Controller()
export class WebfingerController {
  constructor(private readonly webfingerService: WebfingerService) {}

  @Get('/.well-known/webfinger')
  getWebfinger(@Query('resource') resource): Webfinger {
    return this.webfingerService.getWebfinger(resource);
  }
}
