import { Test, TestingModule } from '@nestjs/testing';
import { AppModule } from '../src/app.module';
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';

describe('WebfingerController (e2e)', () => {
  let app: NestFastifyApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication<NestFastifyApplication>(
      new FastifyAdapter(),
    );
    await app.init();
    await app.getHttpAdapter().getInstance().ready();
  });

  it('/.well-known/webfinger (GET)', () => {
    return app
      .inject({
        method: 'GET',
        url: '/.well-known/webfinger',
        query: {
          resource: 'acct:someone@somedomain.com',
        },
      })
      .then((result) => {
        expect(result.statusCode).toEqual(200);
        expect(result.json()).toMatchObject({
          subject: 'acct:someone@somedomain.com',
          links: [
            {
              rel: 'http://openid.net/specs/connect/1.0/issuer',
              href: `${process.env.ISSUER_OIDC_URL}`,
            },
          ],
        });
      });
  });
});
